# Chad Lavimoniere

Hiya, I'm Chad. I'm a Senior Product Designer on the UX Paper Cuts team at GitLab, and a life-long learner based in Brooklyn, NY.

When I'm not at work, some of the things I'm learning about these days are how to play the violin, how to make great backyard pizza, growing a pollinator garden, watercolor painting, and keeping an urban sketch notebook.

(I'm also https://gitlab.com/chadlavimoniere)

<figure>
<img src="https://gitlab.com/clavimoniere/clavimoniere/-/raw/main/chad-about-me.jpeg" height="962" width="1280" />
<figcaption>(I'm the one on the right, in the hat)</figcaption>
</figure>
